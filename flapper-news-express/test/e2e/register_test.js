var expect = require('chai').expect;
var request = require('supertest');
var agent = request.agent(app);
var User = require('../models/Users');
var app = require('../app');

describe('POST register', function() {
    it('user must be registered', function(done) {
        User.findOne({
            username: 'testuser'
        }, function(err, doc) {
            doc.remove().then(function(removed) {
                return res.status(200).send(removed);
            });
        });
        agent.post('/register')
            .send({
                'username': 'testuser',
                'password': 'testpassword'
            })
            .end(function(err, res) {
                if (err) {
                    return done(err);
                }
                expect(res.statusCode).equal(200);
                expect(res.text).be.json;
                var fetchedData = JSON.parse(res.text);
                expect(fetchedData).to.be.an('object');
                expect(fetchedData).to.not.empty;
                expect(fetchedData).to.have.property('token');
                done();
            });
    });
});