var expect = require('chai').expect;
var app = require('../app');
var request = require('supertest');
var agent = request.agent(app);
var User = require('../models/Users');

describe('POST /login', function() {
    it('user should be logged in', function(done) {
        agent.post('/login')
            .send({
                'username': 'mathias',
                'password': 'password'
            })
            .end(function(err, res) {
                if (err) {
                    return done(err);
                }
                expect(res.statusCode).equal(200);
                expect(res.text).be.json;
                var fetchedData = JSON.parse(res.text);
                expect(fetchedData).to.be.an('object');
                expect(fetchedData).to.not.empty;
                expect(fetchedData).to.have.property('token');
                done();
            });
    });
});