var expect = require('chai').expect;
var app = require('../app');
var request = require('supertest');
var agent = request.agent(app);
var Post = require('../models/Posts');

describe('POST post', function() {
    it('post should be added', function(done) {
        Post.findOne({
            title: 'test'
        }, function(err, doc) {
            doc.remove().then(function(removed) {
                return res.status(200).send(removed);
            });
        });
        agent.post('/posts')
            .send({
                'title': 'test',
                'link': 'test'
            })
            .end(function(err, res) {
                if (err) {
                    return done(err);
                }
                expect(res.statusCode).equal(200);
                expect(res.text).be.json;
                var fetchedData = JSON.parse(res.text);
                expect(fetchedData).to.be.an('object');
                expect(fetchedData).to.not.empty;
                done();
            });
    });
});