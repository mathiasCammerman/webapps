describe('Unit: Templates', function() {
    var $httpBackend, location, rootScope;

    beforeEach(module('flapperNews'));
    beforeEach(inject(function(_$rootScope_, _$route_, _$httpBackend_, _$location_) {
        location = _$location_;
        rootScope = _$rootScope_;
        route = _$route_;
        $httpBackend = _$httpBackend_;
    }));

    afterEach(function() {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    it('loads the home template at /home', function() {
        $httpBackend.expectGET('/home.html')
            .respond(200);
        location.path('/home');
        rootScope.$digest();
        $httpBackend.flush();
    });

    it('loads the login template at /login', function() {
        $httpBackend.expectGET('/login.html')
            .respond(200);
        location.path('/login');
        rootScope.$digest();
        $httpBackend.flush();
    });

    it('loads the register template at /register', function() {
        $httpBackend.expectGET('/register.html')
            .respond(200);
        location.path('/register');
        rootScope.$digest();
        $httpBackend.flush();
    });

});