var express = require('express');
var router = express.Router();
var passport = require('passport');
var mongoose = require('mongoose');
var User = mongoose.model('User');
var Post = mongoose.model('Post');
var Comment = mongoose.model('Comment');
var jwt = require('express-jwt');
var auth = jwt({
    secret: 'SECRET',
    userProperty: 'payload'
});

/* GET home page. */
router.get('/', function(req, res) {
    res.render('index', {
        title: 'Express'
    });
});

/* GET voor animations pagina */
router.get('/animations', function(req, res, next) {
    res.render('animations', {
        title: 'Animations'
    });
});

/* GET custom directive pagina */
router.get('/custDirective', function(req, res) {
    res.render('custDirective', {
        title: 'Directive'
    });
});

/* GET promises pagina*/
router.get('/promises', function(req, res) {
    res.render('promises', {
        title: 'Promises'
    });
});

// Alle posts ophalen uit db
router.get('/posts', function(req, res, next) {
    Post.find(function(err, posts) {
        if (err) {
            return next(err);
        }

        res.json(posts);
    });
});
// Een nieuwe post maken en toevoegen aan db
router.post('/posts', auth, function(req, res, next) {
    var post = new Post(req.body);
    post.author = req.payload.username;

    post.save(function(err, post) {
        if (err) {
            return next(err);
        }

        res.json(post);
    });
});

// de id parameter gebruiken om de post te zoeken
router.param('post', function(req, res, next, id) {
    var query = Post.findById(id);

    query.exec(function(err, post) {
        if (err) {
            return next(err);
        }
        if (!post) {
            return next(new Error("can't find post"));
        }

        req.post = post;
        return next();
    });
});

// de id parameter gebruiken om de comment te zoeken
router.param('comment', function(req, res, next, id) {
    var query = Comment.findById(id);

    query.exec(function(err, comment) {
        if (err) {
            return next(err);
        }
        if (!comment) {
            return next(new Error("can't find comment"));
        }

        req.comment = comment;
        return next();
    });
});

// De post ophalen waarop geklikt is
router.get('/posts/:post', function(req, res) {
    req.post.populate('comments', function(err, post) {
        res.json(post);
    });
});

// de score van post verhogen
router.put('/posts/:post/upvote', auth, function(req, res, next) {
    req.post.upvote(function(err, post) {
        if (err) {
            return next(err);
        }

        res.json(post);
    });
});

// de score van post verlagen
router.put('/posts/:post/downvote', auth, function(req, res, next) {
    req.post.downvote(function(err, post) {
        if (err) {
            return next(err);
        }

        res.json(post);
    });
});

// de score van uw comment verhogen
router.put('/posts/:post/comments/:comment/upvote', auth, function(req, res, next) {
    req.comment.upvote(function(err, comment) {
        if (err) {
            return next(err);
        }

        res.json(comment);
    });
});

// de score van uw comment verlagen
router.put('/posts/:post/comments/:comment/downvote', auth, function(req, res, next) {
    req.comment.downvote(function(err, comment) {
        if (err) {
            return next(err);
        }

        res.json(comment);
    });
});

// de comments ophalen die bij de juiste post horen
router.post('/posts/:post/comments', auth, function(req, res, next) {
    var comment = new Comment(req.body);
    comment.post = req.post;
    comment.author = req.payload.username;

    comment.save(function(err, comment) {
        if (err) {
            return next(err);
        }

        req.post.comments.push(comment);
        req.post.save(function(err, post) {
            if (err) {
                return next(err);
            }

            res.json(comment);
        });
    });
});

// een nieuwe gebruiker aanmaken en in de db opslaan
router.post('/register', function(req, res, next) {
    if (!req.body.username || !req.body.password) {
        return res.status(400).json({
            message: 'Please fill out all fields'
        });
    }

    var user = new User();

    user.username = req.body.username;

    user.setPassword(req.body.password)

    user.save(function(err) {
        if (err) {
            return next(err);
        }

        return res.json({
            token: user.generateJWT()
        })
    });
});

// inloggen met uw gebruiker
router.post('/login', function(req, res, next) {
    if (!req.body.username || !req.body.password) {
        return res.status(400).json({
            message: 'Please fill out all fields'
        });
    }

    passport.authenticate('local', function(err, user, info) {
        if (err) {
            return next(err);
        }

        if (user) {
            return res.json({
                token: user.generateJWT()
            });
        } else {
            return res.status(401).json(info);
        }
    })(req, res, next);
});

module.exports = router;