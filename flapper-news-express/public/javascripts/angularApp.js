var app = angular.module('flapperNews', ['ui.router', 'ngAnimate']);

// ui.router config
app.config([
    '$stateProvider',
    '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('home', {
                url: '/home',
                templateUrl: '/home.html',
                controller: 'MainCtrl',
                resolve: {
                    postPromise: ['posts', function(posts) {
                        return posts.getAll();
                    }]
                }
            })

        .state('login', {
                url: '/login',
                templateUrl: '/login.html',
                controller: 'AuthCtrl',
                onEnter: ['$state', 'auth', function($state, auth) {
                    if (auth.isLoggedIn()) {
                        $state.go('home');
                    }
                }]
            })
            .state('register', {
                url: '/register',
                templateUrl: '/register.html',
                controller: 'AuthCtrl',
                onEnter: ['$state', 'auth', function($state, auth) {
                    if (auth.isLoggedIn()) {
                        $state.go('home');
                    }
                }]
            })
            .state('slider', {
                url: '/slider',
                templateUrl: '/slider.html',
                controller: 'sliderCtrl'
            })
            .state('animations', {
                url: '/animations',
                templateUrl: '/animations.html'
            })
            .state('custDirective', {
                url: '/custDirective',
                templateUrl: '/custDirective.html',
                controller: 'SliderController'
            })
            .state('promises', {
                url: '/promises',
                templateUrl: '/promises.html',
                controller: 'HelloCtrl'
            })
            .state('posts', {
                url: '/posts/{id}',
                templateUrl: '/posts.html',
                controller: 'PostsCtrl',
                resolve: {
                    post: ['$stateParams', 'posts', function($stateParams, posts) {
                        return posts.get($stateParams.id);
                    }]
                }
            });

        $urlRouterProvider.otherwise('home');
    }
]);

//my custom directive
app.directive('custom', function() {
    return {
        restrict: 'AE',
        replace: 'true',
        template: '<p>My directive</p>'
    };
});

// Main controller
app.controller('MainCtrl', [
    '$scope',
    'auth',
    'posts',

    function($scope, auth, posts) {

        $scope.posts = posts.posts;
        $scope.isLoggedIn = auth.isLoggedIn;

        $scope.addPost = function() {
            if ($scope.title === '') {
                return;
            }
            posts.create({
                title: $scope.title,
                link: $scope.link,
            });
            $scope.title = '';
            $scope.link = '';
        };

        $scope.incrementUpvotes = function(post) {
            posts.upvote(post);
        };

        $scope.decreaseUpvotes = function(post) {
            posts.downvote(post);
        }
    }
]);

//factory voor promises
app.factory('HelloWorld', function($q, $timeout) {

    var getMessages = function() {
        var deferred = $q.defer();

        $timeout(function() {
            deferred.resolve(['Hello', 'world']);
        }, 2000);

        return deferred.promise;
    };

    return {
        getMessages: getMessages
    };

});

// promises controller
app.controller('HelloCtrl', function($scope, HelloWorld) {

    $scope.messages = HelloWorld.getMessages();

});

// Post controller
app.controller('PostsCtrl', [
    '$scope',
    'auth',
    'posts',
    'post',
    function($scope, auth, posts, post) {
        $scope.post = post;
        $scope.isLoggedIn = auth.isLoggedIn;

        $scope.addComment = function() {
            if ($scope.body === '') {
                return;
            }
            posts.addComment(post._id, {
                body: $scope.body,
                author: 'user',
            }).success(function(comment) {
                $scope.post.comments.push(comment);
            });
            $scope.body = '';
        };
        $scope.incrementUpvotes = function(comment) {
            posts.upvoteComment(post, comment);
        };

        $scope.decreaseUpvotes = function(comment) {
            posts.downvoteComment(post, comment);
        };
    }
]);

app.controller('AuthCtrl', [
    '$scope',
    '$state',
    'auth',
    function($scope, $state, auth) {
        $scope.user = {};

        $scope.register = function() {
            auth.register($scope.user).error(function(error) {
                $scope.error = error;
            }).then(function() {
                $state.go('home');
            });
        };

        $scope.logIn = function() {
            auth.logIn($scope.user).error(function(error) {
                $scope.error = error;
            }).then(function() {
                $state.go('home');
            });
        };
    }
]);

app.controller('NavCtrl', [
    '$scope',
    'auth',
    function($scope, auth) {
        $scope.isLoggedIn = auth.isLoggedIn;
        $scope.currentUser = auth.currentUser;
        $scope.logOut = auth.logOut;
    }
]);

// Angular service
app.factory('posts', ['$http', 'auth', function($http, auth) {
    // service body
    var o = {
        posts: []
    };
    // get all posts
    o.getAll = function() {
        return $http.get('/posts').success(function(data) {
            angular.copy(data, o.posts);
        });
    };
    // create new posts
    o.create = function(post) {
        return $http.post('/posts', post, {
            headers: {
                Authorization: 'Bearer ' + auth.getToken()
            }
        }).success(function(data) {
            o.posts.push(data);
        });
    };
    // upvote
    o.upvote = function(post) {
        return $http.put('/posts/' + post._id + '/upvote', null, {
            headers: {
                Authorization: 'Bearer ' + auth.getToken()
            }
        }).success(function(data) {
            post.upvotes += 1;
        });
    };
    // downvote
    o.downvote = function(post) {
        return $http.put('/posts/' + post._id + '/downvote', null, {
            headers: {
                Authorization: 'Bearer ' + auth.getToken()
            }
        }).success(function(data) {
            post.upvotes -= 1;
        });
    };
    // get single post
    o.get = function(id) {
        return $http.get('/posts/' + id).then(function(res) {
            return res.data;
        });
    };
    // add comment
    o.addComment = function(id, comment) {
        return $http.post('/posts/' + id + '/comments', comment, {
            headers: {
                Authorization: 'Bearer ' + auth.getToken()
            }
        });
    };
    // upvote comment
    o.upvoteComment = function(post, comment) {
        return $http.put('/posts/' + post._id + '/comments/' + comment._id + '/upvote', null, {
            headers: {
                Authorization: 'Bearer ' + auth.getToken()
            }
        }).success(function(data) {
            comment.upvotes += 1;
        });
    };

    o.downvoteComment = function(post, comment) {
        return $http.put('/posts/' + post._id + '/comments/' + comment._id + '/downvote', null, {
            headers: {
                Authorization: 'Bearer ' + auth.getToken()
            }
        }).success(function(data) {
            comment.upvotes -= 1;
        });
    };
    return o;
}]);

app.factory('auth', ['$http', '$window', function($http, $window) {
    var auth = {};

    auth.saveToken = function(token) {
        $window.localStorage['flapper-news-token'] = token;
    };

    auth.getToken = function() {
        return $window.localStorage['flapper-news-token'];
    }

    auth.isLoggedIn = function() {
        var token = auth.getToken();

        if (token) {
            var payload = JSON.parse($window.atob(token.split('.')[1]));

            return payload.exp > Date.now() / 1000;
        } else {
            return false;
        }
    };

    auth.currentUser = function() {
        if (auth.isLoggedIn()) {
            var token = auth.getToken();
            var payload = JSON.parse($window.atob(token.split('.')[1]));

            return payload.username;
        }
    };

    auth.register = function(user) {
        return $http.post('/register', user).success(function(data) {
            auth.saveToken(data.token);
        });
    };

    auth.logIn = function(user) {
        return $http.post('/login', user).success(function(data) {
            auth.saveToken(data.token);
        });
    };

    auth.logOut = function() {
        $window.localStorage.removeItem('flapper-news-token');
    };

    return auth;
}]);

app.directive('createSlider', function($interval) {
    return {
        restrict: 'A',
        scope: {
            links: '=urls',
            current: '=',
            time: '@'
        },
        template: '<ul class="slides" effect-slider>' +
            '<li ng-repeat="url in links track by $index" ng-style="{width: 100/links.length + \'%\'}">' +
            '<img ng-src="{{url}}"/>' +
            '</li>' +
            '</ul>' +
            '<div class="icons">' +
            '<span ng-repeat="icon in links track by $index" ng-class="{\'active\': $index == current}""ng-click="showImg($index)"></span>' +
            '</div>',

        controller: function($scope, $element, $attrs) {
            var intervalID = null;
            var restart = false;
            $scope.showImg = function(index) {
                $scope.current = index;
                $interval.cancel(intervalID);
                restart = false;
                $scope.intervalManager(Number($scope.time) * 1000, restart);
            }

            $scope.intervalManager = function(time, flag) {
                intervalID = $interval(function() {
                    if ($scope.current !== $scope.links.length - 1) {
                        if (!flag) {
                            $scope.current++;
                        } else {
                            if ($scope.current !== 0) {
                                $scope.current--;
                            } else {
                                flag = false;
                                $scope.current++;
                            }
                        }
                    } else {
                        flag = true;
                        $scope.current--;
                    }
                }, time);
            };

            $scope.intervalManager(Number($scope.time) * 1000, restart);

        },
        link: function(scope, elem, attrs) {
            elem.css('width', scope.links.length * 100 + '%');
        }
    };
})

app.directive('effectSlider', function() {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {
            scope.$watch('current', function() {
                elem.css('transform', 'translateX(-' + scope.current * (100 / scope.links.length) + '%)');
            });
        }
    }
})

app.controller('sliderCtrl', ['$scope', function($scope) {
    $scope.current = 0;

}]);

app.controller('SliderController', function($scope) {
    $scope.images = [{
        src: 'img1.png',
        title: 'Pic 1'
    }, {
        src: 'img2.jpg',
        title: 'Pic 2'
    }, {
        src: 'img3.jpg',
        title: 'Pic 3'
    }, {
        src: 'img4.png',
        title: 'Pic 4'
    }, {
        src: 'img5.png',
        title: 'Pic 5'
    }];
});

app.directive('slider', function($timeout) {
    return {
        restrict: 'AE',
        replace: true,
        scope: {
            images: '='
        },
        link: function(scope, elem, attrs) {

            scope.currentIndex = 0;

            scope.next = function() {
                scope.currentIndex < scope.images.length - 1 ? scope.currentIndex++ : scope.currentIndex = 0;
            };

            scope.prev = function() {
                scope.currentIndex > 0 ? scope.currentIndex-- : scope.currentIndex = scope.images.length - 1;
            };

            scope.$watch('currentIndex', function() {
                scope.images.forEach(function(image) {
                    image.visible = false;
                });
                scope.images[scope.currentIndex].visible = true;
            });

            /* Start: For Automatic slideshow*/

            var timer;

            var sliderFunc = function() {
                timer = $timeout(function() {
                    scope.next();
                    timer = $timeout(sliderFunc, 5000);
                }, 5000);
            };

            sliderFunc();

            scope.$on('$destroy', function() {
                $timeout.cancel(timer);
            });

            /* End : For Automatic slideshow*/

        },
        templateUrl: '/templates/templateurl.html'
    }
});